from tkinter import *

if __name__=="__main__":
    
    #Dimención y configuración de la ventana principal
    ven_prin=Tk() 
    ven_prin.title("Proyecto Final")
    ven_prin.geometry("900x600")
    
    #Etiqueta principal
    lbl_cap_dat=Label(ven_prin,text="\nCaptura de datos",font=("None", 12, "bold"))
    
    #Boton y Caja de texto de archivo
    btnArchivo=Button(ven_prin,text="Archivo")    
    txtArchivo=Entry(ven_prin)
    
    #Etiqueta y Caja de texto de N1 y T
    lblN1=Label(ven_prin,text="N1:",font=("None", 10, "bold"))
    txtN1=Entry(ven_prin)
    lblT=Label(ven_prin,text="T:",font=("None", 10, "bold"))
    txtT=Entry(ven_prin)
    
    #Etiqueta y Caja de texto de N2 y Disminuir T
    lblN2=Label(ven_prin,text="N2:",font=("None", 10, "bold"))
    txtN2=Entry(ven_prin)
    lblDT=Label(ven_prin,text="Disminuir T:",font=("None", 10, "bold"))
    txtDT=Entry(ven_prin)    
    
    #Creación de la Tabla 1
    tabla_1=Frame()
    tabla_1.config(bg="gray")
    
    #Creación de la Tabla 2
    tabla_2=Frame()
    tabla_2.config(bg="blue")
    
    #Recorrido y llenado de la tabla 1
    for f1 in range(0, 10):             #número de filas
        for c1 in range(0, 10):         #número de columnas
            cell = Entry(tabla_1, width=3)      #inserción de campo de texto en el Frame
            cell.grid(padx=5, pady=5, row=f1, column=c1)    #Distribución del campo de texto en el Frame
            cell.insert(0, '{}'.format(f1+c1))      #Ingreso del dato en el Campo de texto
        
    #Recorrido y llenado de la tabla 2
    for f2 in range(0, 10):
        for c2 in range(0, 10):
            cell = Entry(tabla_2, width=3)
            cell.grid(padx=5, pady=5, row=f2, column=c2)
            cell.insert(0, '{}'.format(f2+c2))
    
    #Boton Ejeccutar, etiquetas de Costo y Permutación
    btnEjecutar=Button(ven_prin,text="Ejecutar")
    lblCosto=Label(ven_prin,text="Costo:",font=("None", 10, "bold"))
    lblValCosto=Label(ven_prin,text="00000",font=("None", 10, "bold"))
    lblPermutacion=Label(ven_prin,text="Permutación:",font=("None", 10, "bold"))
    lblValPermutacion=Label(ven_prin,text="00000",font=("None", 10, "bold"))
    
    #Posición de boton y etiqueta de Archivo    
    btnArchivo.place(x=10,y=100)
    txtArchivo.place(x=80,y=104,width=600, height=20)
    
    #Posición de etiqueta y campo de texto de N1 y T
    lblN1.place(x=10,y=150)
    txtN1.place(x=50,y=150,width=100, height=20)
    lblT.place(x=460,y=150)
    txtT.place(x=560,y=150,width=100, height=20)
    
    #Posición de etiqueta y campo de texto de N2 y Disminuir T
    lblN2.place(x=10,y=190)
    txtN2.place(x=50,y=190,width=100, height=20)
    lblDT.place(x=460,y=190)
    txtDT.place(x=560,y=190,width=100, height=20)
    
    #Posición de tabla 1 y tabla 2
    tabla_1.place(x=10,y=230)
    tabla_2.place(x=460,y=230)
    
    #Posición de Boton Ejecutar y etiquetas de Costo y permutación
    btnEjecutar.place(x=10,y=530)
    lblCosto.place(x=100,y=530)
    lblValCosto.place(x=150,y=530)    
    lblPermutacion.place(x=100,y=550)
    lblValPermutacion.place(x=200,y=550)
    
    lbl_cap_dat.pack()
    
    ven_prin.mainloop()
